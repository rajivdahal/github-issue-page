import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { rootReducer } from "./src/reducers";

const middleware = [thunk];

const initialValues = {
  issuesData: {
    fetching: false,
    issues: {
      pageNumber: 1,
      data: [],
    },
    error: null,
  },
  issueDetails: {
    fetching: false,
    issue: {},
    error: null,
  },
};

export const store = createStore(
  rootReducer,
  initialValues,
  compose(applyMiddleware(...middleware))
);
