import axios from "axios";
import { GIT_ISSUE_ENDPOINT } from "./constants";
export function fetchIssueDetail(id) {
  return axios({
    method: "get",
    url: GIT_ISSUE_ENDPOINT + `/${id}`,
  });
}
