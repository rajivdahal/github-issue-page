import { fetchIssues } from "../api/fetchIssues";

export const issueDataActionTypes = {
  SET_IS_ISSUEDATA_FETCHING: "SET_IS_ISSUEDATA_FETCHING",
  SET_IS_ISSUEDATA_FETCHED: "SET_IS_ISSUEDATA_FETCHED",
  SET_IS_ISSUEDATA_FETCHED_ERROR: "SET_IS_ISSUEDATA_FETCHED_ERROR",
};

export const fetchIssueData = (params) => (dispatch) => {
  dispatch({
    type: issueDataActionTypes.SET_IS_ISSUEDATA_FETCHING,
    payload: params,
  });
  fetchIssues(params)
    .then((resp) => {
      dispatch({
        type: issueDataActionTypes.SET_IS_ISSUEDATA_FETCHED,
        payload: {
          pageNumber: params,
          data: resp.data,
        },
      });
    })
    .catch((err) => {
      dispatch({
        type: issueDataActionTypes.SET_IS_ISSUEDATA_FETCHED_ERROR,
        error: err.response || "Something went wrong",
      });
    });
};
