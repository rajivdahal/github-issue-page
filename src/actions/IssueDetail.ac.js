import { fetchIssueDetail } from "../api/fetchIssueDetail";

export const issueDetailsActionTypes = {
  SET_IS_ISSUEDETAILS_FETCHING: "SET_IS_ISSUEDETAILS_FETCHING",
  SET_IS_ISSUEDETAILS_FETCHED: "SET_IS_ISSUEDETAILS_FETCHED",
  SET_IS_ISSUEDETAILS_FETCHED_ERROR: "SET_IS_ISSUEDETAILS_FETCHED_ERROR",
};
export const fetchIssueDetails = (params) => (dispatch) => {
  dispatch({
    type: issueDetailsActionTypes.SET_IS_ISSUEDETAILS_FETCHING,
    payload: params,
  });
  fetchIssueDetail(params)
    .then((resp) => {
      dispatch({
        type: issueDetailsActionTypes.SET_IS_ISSUEDETAILS_FETCHED,
        payload: resp.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: issueDetailsActionTypes.SET_IS_ISSUEDETAILS_FETCHED_ERROR,
        error: err.response || "Something went wrong",
      });
    });
};
