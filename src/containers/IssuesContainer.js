/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Issue from "../components/issues-table/Issue";
import LoaderComponent from "../components/commons/LoaderComponent";
import SomethingWentWrong from "../components/commons/SomethingWentWrong";
import { Pagination } from "react-bootstrap";
import { bindActionCreators } from "redux";
import { fetchIssueData } from "../actions/IssueData.ac";
import { useDispatch, useSelector } from "react-redux";
const IssuesContainerWrapper = styled.div`
  border: 1px solid #e1e4e8;
  border-collapse: collapse;
`;

export default function IssuesContainer() {
  const dispatch = useDispatch();
  const fetchData = bindActionCreators(fetchIssueData, dispatch);
  const { issuesData } = useSelector((store) => store);
  let { fetching, error, issues } = issuesData;
  console.log("error is", fetching, error);
  useEffect(() => {
    let isMounted = true;
    if (isMounted) fetchData(1);
    return () => {
      isMounted = false;
    };
  }, []);
  return (
    <div>
      <div>
        {fetching ? (
          <LoaderComponent />
        ) : error ? (
          <SomethingWentWrong />
        ) : (
          <IssuesContainerWrapper>
            {!!issues &&
              issues.data.map((issue) => (
                <Issue key={issue.id} issue={issue} />
              ))}
          </IssuesContainerWrapper>
        )}
      </div>
      <div
        style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}
      >
        <Pagination>
          {[...new Array(10).fill(Math.random())].map((_, index) => {
            return (
              <Pagination.Item
                onClick={() => fetchData(index + 1)}
                key={index + 1}
                active={index + 1 === issues.pageNumber}
              >
                {index + 1}
              </Pagination.Item>
            );
          })}
        </Pagination>
      </div>
    </div>
  );
}
