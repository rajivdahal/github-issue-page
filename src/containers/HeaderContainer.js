/* eslint-disable no-nested-ternary */
import React, { Component, useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import { fetchRepoInfoData } from "../api/fetchRepoInfo";
import Header from "../components/header/Header";
import LoaderComponent from "../components/commons/LoaderComponent";
import SomethingWentWrong from "../components/commons/SomethingWentWrong";

import { bindActionCreators } from "redux";

export default function HeaderContainer(props) {
  const fetching = false;
  const error = false;
  const repoInfo = {
    name: "name",
    html_url: "url",
    stargazers_count: 1,
    subscribers_count: 2,
    forks_count: 5,
  };
  return (
    <div>
      <div>
        {fetching ? (
          <LoaderComponent />
        ) : error ? (
          <SomethingWentWrong />
        ) : (
          repoInfo &&
          Object.keys(repoInfo).length > 0 && <Header {...repoInfo} />
        )}
      </div>
    </div>
  );
}
