import { issueDetailsActionTypes } from "../actions/IssueDetail.ac";

export const IssueDetailsReducer = (state, action) => {
  switch (action.type) {
    case issueDetailsActionTypes.SET_IS_ISSUEDETAILS_FETCHING:
      return {
        ...state,
        fetching: true,
      };
    case issueDetailsActionTypes.SET_IS_ISSUEDETAILS_FETCHED:
      return {
        ...state,
        fetching: false,
        issue: action.payload,
      };
    case issueDetailsActionTypes.SET_IS_ISSUEDETAILS_FETCHED_ERROR:
      return {
        ...state,
        error: true,
      };
    default:
      return {
        ...state,
      };
  }
};
