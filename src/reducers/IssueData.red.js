import { issueDataActionTypes } from "../actions/IssueData.ac";

export const IssueDataReducer = (state, action) => {
  switch (action.type) {
    case issueDataActionTypes.SET_IS_ISSUEDATA_FETCHING:
      return {
        ...state,
        fetching: true,
      };
    case issueDataActionTypes.SET_IS_ISSUEDATA_FETCHED:
      return {
        ...state,
        fetching: false,
        issues: {
          pageNumber: action.payload.pageNumber,
          data: action.payload.data,
        },
      };
    case issueDataActionTypes.SET_IS_ISSUEDATA_FETCHED_ERROR:
      return {
        ...state,
        error: true,
        fetching: false,
      };
    default:
      return {
        ...state,
      };
  }
};
