import { combineReducers } from "redux";
import { IssueDataReducer } from "./IssueData.red";
import { IssueDetailsReducer } from "./IssueDetails.red";

export const rootReducer = combineReducers({
  issuesData: IssueDataReducer,
  issueDetails: IssueDetailsReducer,
});
