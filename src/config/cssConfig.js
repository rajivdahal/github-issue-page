export const colors = {
  blue: "#0969da",
  lightGrayText: "#24292f",
  colorFgMuted: "#57606a",
  colorBorderDefault: "#d0d7de",
  colorBtnText: "#24292f",
  colorBtnBorder: "rgba(27,31,36,0.15)",
  colorBtnCounterBg: "rgba(27,31,36,0.08)",
  colorPrimaryBorderActive: "#fd8c73",
  SVGColor: "#24292f",
  colorBtnPrimaryBg: "#2da44e",
  colorBorderDefault: "#d0d7de",
};
