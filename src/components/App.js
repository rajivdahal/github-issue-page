import React from "react";
import styled from "styled-components";
import SubNav from "./search-subnav/SubNav";
import HeaderContainer from "../containers/HeaderContainer";
import IssuesTable from "./issues-table/IssuesTable";
import IssuesDetail from "./issue-detail/IssueDetail";
import { BrowserRouter, Route, Routes } from "react-router-dom";

const Container = styled.div`
  font-size: 14px;
  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial,
    sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol;
  margin: 0px;
  padding: 0px;
`;

const IssueListingContainer = styled.div`
  font-size: 14px;
  width: 75%;
  margin: 0 auto;
`;

const App = () => (
  <BrowserRouter>
    <Container>
      <HeaderContainer />
      <IssueListingContainer>
        <Routes>
          <Route path="/" element={<IssuesTable />} />
          <Route path="issue/:id" element={<IssuesDetail />} />
          <Route path="*" element={<IssuesTable />} />
        </Routes>
      </IssueListingContainer>
    </Container>
  </BrowserRouter>
);
export default App;
