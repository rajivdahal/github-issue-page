import styled from "styled-components";
import { colors } from "../../../config/cssConfig";

const CommonSVGStyle = styled.svg`
  color: ${(props) =>
    props.color ? props.color : !props.selected ? colors.SVGColor : "#000000"};
  fill: currentColor;
  margin-right: 5px;
  display: inline-block;

  &:hover {
    color: ${(props) => props.hoverColor && props.hoverColor};
  }
`;

export default CommonSVGStyle;
