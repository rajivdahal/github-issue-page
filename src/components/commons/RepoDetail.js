import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import WatchSVG from "./svg/WatchSVG";
import StarSVG from "./svg/StarSVG";
import ForkSVG from "./svg/ForkSVG";
import Caret from "./Caret";
import Detail from "./Detail";
import NotificationSvg from "./svg/notificationSvg";
import { colors } from "../../config/cssConfig";

const RepoDetailContainer = styled.div`
  display: inline-block;
  margin: 5px;
  font-size: 12px;
  border-collapse: collapse;
`;

const RepoDetailTagContainer = styled.span`
  border: 1px solid ${colors.colorBtnBorder};
  border-radius: 7px;
  background-color: #f4f7f9;
  line-height: 20px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  color: ${colors.colorBtnText}!important;
  padding: 6px;
  /* width: ${(props) => (props.tag === "Star" ? "200px" : "")}; */
  cursor: pointer;
  span {
    padding: 0px 5px;
  }
  span:nth-child(3) {
    background-color: ${colors.colorBtnCounterBg};
    border-radius: 30px;
    padding: 0px 6px;
    margin: ${(props) => (props.tag === "Star" ? "0px 10px 0px 0px" : "0px")};
    min-width: 20px;
    text-align: center;
  }
  svg {
    fill: ${colors.lightGrayText};
  }
`;

const RepoDetailValueContainer = styled.span`
  border: 1px solid #cdcfd1;
  padding: 5px 10px;
  font-weight: bold;
  border-radius: 0px 2px 2px 0px;
  line-height: 20px;
  cursor: pointer;
  background-color: #ffffff;
`;

const RepoDetail = ({ tag, value }) => (
  <RepoDetailContainer>
    <RepoDetailTagContainer tag={tag}>
      {tag === "Notifications" && <NotificationSvg />}
      {tag === "Star" && <StarSVG />}
      {tag === "Fork" && <ForkSVG />}

      <span>{tag}</span>
      {value && <span>{value}</span>}
      {tag === "Star" && (
        <div
          style={{
            borderLeft: `1px solid ${colors.colorBtnBorder}`,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            padding: "0px 6px",
          }}
        >
          <Caret></Caret>
        </div>
      )}
    </RepoDetailTagContainer>
  </RepoDetailContainer>
);

export default RepoDetail;

Detail.propTypes = {
  tag: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};
