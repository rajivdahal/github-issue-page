import styled from "styled-components";
import React from "react";
import PropTypes from "prop-types";
import CodeSVG from "../commons/svg/CodeSVG";
import IssueOpenedSVG from "../commons/svg/IssueOpenedSVG";
import PrSVG from "../commons/svg/PrSVG";
import ProjectSVG from "../commons/svg/ProjectSVG";
import InsightsSVG from "../commons/svg/InsightsSVG";
import { colors } from "../../config/cssConfig";

const NavigationItemWrapper = styled.div`
  border-radius: 3px 3px 0 0;
  border-bottom: 2px solid
    ${(props) => (props.selected ? "red" : "transparent")};
  color: ${(props) =>
    props.selected ? colors.colorPrimaryBorderActive : "#586069"};
  padding: 7px 15px 7px;
  white-space: nowrap;
  background-color: ${(props) => props.selected && "#fff"};
  /* border-color: ${(props) =>
    props.selected && "#e36209 #e1e4e8 transparent"}; */
  cursor: pointer;
  display: flex;
  justify-content: space-between;
  align-items: center;
  &:hover {
    color: #000;
  }
`;

export const NavigationItemValue = styled.span`
  background-color: ${colors.colorBtnCounterBg};
  border-radius: 30px;
  padding: 0px 8px;
  min-width: 20px;
  text-align: center;
  color: ${colors.colorBtnText};
  margin-left: 10px;
`;

export const NavigationItemAnchor = styled.a`
  text-decoration: none;
  color: #586069;
  &:hover {
    color: #586069;
  }
`;
const ItemsNameAndBagde = styled.div`
  border-radius: 5px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 2px 5px;
  &:hover {
    background-color: ${colors.colorBorderDefault};
  }
`;
const NavigationItem = ({ name, selected, value, url }) => (
  <NavigationItemWrapper selected={selected}>
    <ItemsNameAndBagde>
      {name === "Code" && <CodeSVG />}
      {name === "Issues" && <IssueOpenedSVG />}
      {name === "Pull Requests" && <PrSVG />}
      {name === "Projects" && <ProjectSVG />}
      {name === "Insights" && <InsightsSVG />}
      <NavigationItemAnchor href={url}>{name}</NavigationItemAnchor>
      {value ? (
        <NavigationItemValue>{value}</NavigationItemValue>
      ) : (
        <span>{value}</span>
      )}
    </ItemsNameAndBagde>
  </NavigationItemWrapper>
);

export default NavigationItem;
