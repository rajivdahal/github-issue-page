import React from "react";
import styled from "styled-components";
import IssuesTableHeader from "./IssuesTableHeader";
import IssuesContainer from "../../containers/IssuesContainer";
import SubNav from "../search-subnav/SubNav";

const IssuesTableWraper = styled.div`
  margin-bottom: 20px;
`;
const IssuesTable = () => (
  <>
    <SubNav />
    <IssuesTableWraper>
      <IssuesTableHeader />
      <IssuesContainer />
    </IssuesTableWraper>
  </>
);

export default IssuesTable;
