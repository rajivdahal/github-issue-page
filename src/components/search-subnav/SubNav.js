import React from "react";
import styled from "styled-components";
import Detail from "../commons/Detail";
import SearchSVG from "../commons/svg/SearchSVG";
import Caret from "../commons/Caret";
import Input from "./Input";
import FiltersContainer from "./FiltersContainer";
import CreateNewIssueButton from "./CreateNewIssueButton";
import { OWNER, REPO, WEB_URL } from "../../api/constants";

const SubNavContainer = styled.div`
  display: grid;
  align-items: baseline;
  flex-direction: row;
  grid-template-columns: 70% 30%;
  @media (max-width: 820px) {
    grid-template-rows: auto;
  }
`;

const SearchContainer = styled.div`
  display: inline-block;
  position: relative;
  width: 100%;

  @media (max-width: 820px) {
    margin-top: 10px;
  }
`;

const FilterNSearchContainer = styled.div`
  display: flex;

  @media (max-width: 820px) {
    flex-direction: column;
  }
`;
const SubNav = () => (
  <SubNavContainer>
    <FilterNSearchContainer>
      <SearchContainer>
        <Input type="text" defaultValue="is:issue is:open " />
        <SearchSVG />
      </SearchContainer>
    </FilterNSearchContainer>

    <div>
      <Detail
        tag="Labels"
        value="Milestones"
        tagBackgroundColor="#ffffff"
        valueBackgroundColor="#ffffff"
        tagPadding="9px 14px"
        valuePadding="9px 14px"
        tagHref={`${WEB_URL}/${OWNER}/${REPO}/labels`}
        valueHref={`${WEB_URL}/${OWNER}/${REPO}/milestones`}
      />
      <CreateNewIssueButton href={`${WEB_URL}/${OWNER}/${REPO}/issues/new`}>
        New issue
      </CreateNewIssueButton>
    </div>
  </SubNavContainer>
);

export default SubNav;
